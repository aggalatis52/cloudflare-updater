# Set the base image to use for the container
FROM arm32v5/python:3.11-slim-bullseye

# Set the working directory in the container
WORKDIR /app

# Copy the requirements file to the working directory
COPY requirements.txt .

# Install the required packages using pip
RUN pip install --no-cache-dir -r requirements.txt

# Copy the application files to the working directory
COPY . .

# Set the command to run when the container starts
ENTRYPOINT ["python", "main.py"]
