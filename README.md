# Cloudflare Updater

A Python script that updates a Cloudflare DNS A record with your current public IP address.

## Getting Started

### Prerequisites

- Python 3.x
- Pip

### Installing

1. Clone this repository: `https://gitlab.com/aggalatis52/cloudflare-updater.git`
2. Install required packages: `pip install -r requirements.txt`

### Environment

Create a `.env` file containg the following variables
| Variable Name | Description|
| ------------- | ------------- |
| CLOUDFLARE_API_TOKEN | **string** Your cloudflare api token with access to zone dns records |
| DOMAIN_NAME | **string** Your domain (zone) name |
| DNS_RECORD_NAME | **string** Your dns record name to be updated |
| UPDATE_MINUTES_INTERVAL | **integer** How much time to wait until the updater re starts |

### Usage

Run the script: `python main.py`
