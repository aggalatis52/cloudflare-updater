import os
import sys
import time
from datetime import datetime
from classes.apiProvider import ApiProvider

class CloudflareUpdater():
    def __init__(self):
        self.apiProvider = ApiProvider()
        self.domain_name = os.getenv("DOMAIN_NAME")
        self.dns_record_name = os.getenv("DNS_RECORD_NAME")
        self.domain_id = None
        self.dns_record_id = None
        self.restart_interval = int(os.getenv("UPDATE_MINUTES_INTERVAL")) * 60
        self.start()

    def start(self):
        self.find_mandatory_ids()
        while (True):
            try:
                self.run_update_process()
                time.sleep(self.restart_interval)
            except Exception as exp:
                print(exp)
                time.sleep(self.restart_interval)

    
    def run_update_process(self):
        print("Running update process...")
        external_ip = self.apiProvider.get_external_ip_address()
        response_status, response_body = self.apiProvider.update_cloudflare_dns_record_ip_address(self.domain_id, self.dns_record_id, self.dns_record_name, external_ip)
        if response_status != 200:
            Exception(f'[ERROR] Cloudflare responded with status code {response_status}')
        print(f"{datetime.now()} DNS record {response_body['result']['name']} content updated to: {response_body['result']['content']}")


    def find_mandatory_ids(self):
        try:
            self.domain_id = self.find_domain_id(self.domain_name)
            self.dns_record_id = self.find_dns_record_id(self.domain_id, self.dns_record_name)
        except Exception as exp:
            print(exp)
            sys.exit()


    def find_domain_id(self, domain_name):
        zones = self.apiProvider.get_cloudflare_zones()
        for zone in zones['result']:
            if zone['name'] == domain_name: return zone['id']
        raise Exception('[ERROR] Unable to locate domain inside cloudflare zones.')
    

    def find_dns_record_id(self, domain_id, dns_record_name):
        dns_records = self.apiProvider.get_cloudflare_dns_records(domain_id)
        for dns_record in dns_records['result']:
            if dns_record['name'] == dns_record_name and dns_record['type'] == "A": return dns_record['id']
        raise Exception('[ERROR] Unable to locate dns record inside domain records.') 
        