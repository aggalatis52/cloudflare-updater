import os
import requests


class ApiProvider():
    _instance = None
    def __new__(self):
        if self._instance is None:
            self.cloudflare_api = "https://api.cloudflare.com/client/v4"
            self.cloudflare_api_token = os.getenv("CLOUDFLARE_API_TOKEN")
            self.ipinfo_url = "https://ipinfo.io/ip"
            self.cloudflare_headers = {
                'Authorization': f"Bearer {self.cloudflare_api_token}"
            }
            self._instance = super(ApiProvider, self).__new__(self)
        return self._instance
    
    def get_external_ip_address(self):
        response = requests.get(self.ipinfo_url, timeout=20)
        return response.text


    def get_cloudflare_zones(self):
        response = requests.get(self.cloudflare_api + '/zones', headers=self.cloudflare_headers)
        return response.json()
    
    def get_cloudflare_dns_records(self, zone_id):
        response = requests.get(self.cloudflare_api + f'/zones/{zone_id}/dns_records', headers=self.cloudflare_headers)
        return response.json()

    def update_cloudflare_dns_record_ip_address(self, zone_id, dns_record_id, dns_record_name, ip_address):
        payload = {
            "content": ip_address,
            "name": dns_record_name,
            "type": "A"
        }
        response = requests.patch(self.cloudflare_api + f'/zones/{zone_id}/dns_records/{dns_record_id}', headers=self.cloudflare_headers, json=payload)
        return response.status_code, response.json()